import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleCardsListComponent } from './clash-royale-cards-list.component';

describe('ClashRoyaleCardsListComponent', () => {
  let component: ClashRoyaleCardsListComponent;
  let fixture: ComponentFixture<ClashRoyaleCardsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleCardsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleCardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
