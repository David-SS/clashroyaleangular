import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card} from '../Models/card_model';
import {compare} from 'semver';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-clash-royale-cards-list',
  templateUrl: './clash-royale-cards-list.component.html',
  styleUrls: ['./clash-royale-cards-list.component.css']
})
export class ClashRoyaleCardsListComponent implements OnInit {

  @Input() cardList: Card[];
  @Input() refreshBar: Subject<void>;
  sortMode: string;

  @Output() onListCardSelectedEvent: EventEmitter<Card>;

  constructor() {
    this.onListCardSelectedEvent = new EventEmitter();
    this.sortMode = '';
  }

  private static getElixirSort(a: Card, b: Card) {
    let comparator: number;
    comparator = a.elixir - b.elixir;
    return comparator;
  }

  private static getInverseRaritySort(a: Card, b: Card) {
    let comparator: number;
    comparator = a.getRarityIndex() - b.getRarityIndex();
    if (comparator === 0) {
      return ClashRoyaleCardsListComponent.getElixirSort(a, b);
    } else {
      return comparator;
    }
  }

  private static getRaritySort(a: Card, b: Card) {
    let comparator: number;
    comparator = b.getRarityIndex() - a.getRarityIndex();
    if (comparator === 0) {
      return ClashRoyaleCardsListComponent.getElixirSort(a, b);
    } else {
      return comparator;
    }
  }

  ngOnInit() {
    // Empty function... add function when something changes
    this.refreshBar.subscribe(() => {});
  }

  private popElementOfCardList(card: Card) {
    const index: number = this.cardList.indexOf(card);
    if (index !== -1) {
      this.cardList.splice(index, 1);
    }
  }

  onChange(value: string): void {
    this.sortMode = value;
  }

  sortedCardList(): Card[] {
    return this.cardList.sort((a: Card, b: Card) => {
      if (this.sortMode === 'elixir') { return ClashRoyaleCardsListComponent.getElixirSort(a, b); }
      if (this.sortMode === 'irarity') { return ClashRoyaleCardsListComponent.getInverseRaritySort(a, b); }
      return ClashRoyaleCardsListComponent.getRaritySort(a, b);
    });
  }

  onCardSelectedEmitter(card: Card) {
    this.popElementOfCardList(card);
    this.onListCardSelectedEvent.emit(card);
  }

}
