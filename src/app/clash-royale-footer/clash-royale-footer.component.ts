import {Component, Input, OnInit} from '@angular/core';
import {Deck} from '../Models/deck_model';

@Component({
  selector: 'app-clash-royale-footer',
  templateUrl: './clash-royale-footer.component.html',
  styleUrls: ['./clash-royale-footer.component.css']
})
export class ClashRoyaleFooterComponent implements OnInit {

  @Input() deckList: Deck[];

  constructor() {
  }

  ngOnInit() {
  }

}
