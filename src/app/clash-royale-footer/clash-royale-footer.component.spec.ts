import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleFooterComponent } from './clash-royale-footer.component';

describe('ClashRoyaleFooterComponent', () => {
  let component: ClashRoyaleFooterComponent;
  let fixture: ComponentFixture<ClashRoyaleFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
