import {Component, Input, OnInit} from '@angular/core';
import {Deck} from '../Models/deck_model';

@Component({
  selector: 'app-clash-royale-deck-row',
  templateUrl: './clash-royale-deck-row.component.html',
  styleUrls: ['./clash-royale-deck-row.component.css']
})
export class ClashRoyaleDeckRowComponent implements OnInit {

  @Input() currentDeck: Deck;

  constructor() {
  }

  ngOnInit() {
  }

}
