import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleDeckRowComponent } from './clash-royale-deck-row.component';

describe('ClashRoyaleDeckRowComponent', () => {
  let component: ClashRoyaleDeckRowComponent;
  let fixture: ComponentFixture<ClashRoyaleDeckRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleDeckRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleDeckRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
