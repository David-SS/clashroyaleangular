import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ClashRoyaleHeaderComponent } from './clash-royale-header/clash-royale-header.component';
import { ClashRoyaleDeckListComponent } from './clash-royale-deck-list/clash-royale-deck-list.component';
import { ClashRoyaleDeckRowComponent } from './clash-royale-deck-row/clash-royale-deck-row.component';
import { ClashRoyaleFooterComponent } from './clash-royale-footer/clash-royale-footer.component';
import { ClashRoyaleCardsPanelComponent } from './clash-royale-cards-panel/clash-royale-cards-panel.component';
import { ClashRoyaleCardsListComponent } from './clash-royale-cards-list/clash-royale-cards-list.component';
import { ClashRoyaleDeckEditorComponent } from './clash-royale-deck-editor/clash-royale-deck-editor.component';
import { ClashRoyaleCardItemComponent } from './clash-royale-card-item/clash-royale-card-item.component';
import { ClashRoyaleDeckFormComponent } from './clash-royale-deck-form/clash-royale-deck-form.component';
import { ClashRoyaleDeckPreviewComponent } from './clash-royale-deck-preview/clash-royale-deck-preview.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {ProgressBarModule} from 'angular-progress-bar';
import {FormsModule} from '@angular/forms';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireDatabaseModule} from '@angular/fire/database';
// import {environment} from '../environments/environment';
import {environment} from '../environments/environment.prod';

@NgModule({
  declarations: [
    AppComponent,
    ClashRoyaleHeaderComponent,
    ClashRoyaleDeckListComponent,
    ClashRoyaleDeckRowComponent,
    ClashRoyaleFooterComponent,
    ClashRoyaleCardsPanelComponent,
    ClashRoyaleCardsListComponent,
    ClashRoyaleDeckEditorComponent,
    ClashRoyaleCardItemComponent,
    ClashRoyaleDeckFormComponent,
    ClashRoyaleDeckPreviewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    ProgressBarModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
