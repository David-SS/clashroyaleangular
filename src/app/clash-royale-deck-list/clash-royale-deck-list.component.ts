import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Deck} from '../Models/deck_model';

@Component({
  selector: 'app-clash-royale-deck-list',
  templateUrl: './clash-royale-deck-list.component.html',
  styleUrls: ['./clash-royale-deck-list.component.css']
})
export class ClashRoyaleDeckListComponent implements OnInit {

  @Input() deckList: Deck[];
  @Output() onSelectedDeckEvent: EventEmitter<Deck>;

  constructor() {
    this.onSelectedDeckEvent = new EventEmitter();
  }

  ngOnInit() {
  }

  onSelectedDeckEmitter(deck: Deck) {
    this.onSelectedDeckEvent.emit(deck);
  }

}
