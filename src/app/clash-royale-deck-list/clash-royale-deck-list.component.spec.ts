import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleDeckListComponent } from './clash-royale-deck-list.component';

describe('ClashRoyaleDeckListComponent', () => {
  let component: ClashRoyaleDeckListComponent;
  let fixture: ComponentFixture<ClashRoyaleDeckListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleDeckListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleDeckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
