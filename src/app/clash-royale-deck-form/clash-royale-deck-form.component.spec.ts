import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleDeckFormComponent } from './clash-royale-deck-form.component';

describe('ClashRoyaleDeckFormComponent', () => {
  let component: ClashRoyaleDeckFormComponent;
  let fixture: ComponentFixture<ClashRoyaleDeckFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleDeckFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleDeckFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
