import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Deck} from '../Models/deck_model';

declare var $: any;

@Component({
  selector: 'app-clash-royale-deck-form',
  templateUrl: './clash-royale-deck-form.component.html',
  styleUrls: ['./clash-royale-deck-form.component.css']
})
export class ClashRoyaleDeckFormComponent implements OnInit {

  @Input() currentDeck: Deck;

  @Output() onDeleteAndResetDeckEvent: EventEmitter<Deck>;
  @Output() onSaveDeckEvent: EventEmitter<Deck>;

  constructor() {
    this.onDeleteAndResetDeckEvent = new EventEmitter();
    this.onSaveDeckEvent = new EventEmitter();
  }

  ngOnInit() {
    $('.ui.dropdown').dropdown();
  }

  onDeleteDeckEmitter() {
    if (this.currentDeck.id !== -1) {
      this.onDeleteAndResetDeckEvent.emit(this.currentDeck);
    } else {
      alert('Debe seleccionar un mazo que borrar');
    }
  }

  onSaveDeckEmitter() {
    if (this.currentDeck.getCardsCount() === 8 && this.currentDeck.name.length > 0) {
      if (this.currentDeck.id === -1) {
        console.log(this.currentDeck);
        this.onSaveDeckEvent.emit(this.currentDeck);
      } else {
        this.onSaveDeckEvent.emit(this.currentDeck);
      }
    } else {
      alert('Debe completar el mazo');
    }
  }

  onResetDeckEmitter() {
    this.onDeleteAndResetDeckEvent.emit(new Deck());
  }

}
