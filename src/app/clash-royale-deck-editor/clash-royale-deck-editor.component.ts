import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Deck} from '../Models/deck_model';
import {Card} from '../Models/card_model';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-clash-royale-deck-editor',
  templateUrl: './clash-royale-deck-editor.component.html',
  styleUrls: ['./clash-royale-deck-editor.component.css']
})
export class ClashRoyaleDeckEditorComponent implements OnInit {

  @Input() currentDeck: Deck;
  @Input() cardList: Card[];
  @Input() refreshBar: Subject<Deck>;

  @Output() onDeleteAndResetDeckEvent: EventEmitter<Deck>;
  @Output() onSaveDeckEvent: EventEmitter<Deck>;

  constructor() {
    this.onDeleteAndResetDeckEvent = new EventEmitter();
    this.onSaveDeckEvent = new EventEmitter();
  }

  ngOnInit() {
    this.refreshBar.subscribe((deck) => {
      this.currentDeck = deck;
      this.currentDeck.stats.checkDeck(this.currentDeck);
    });
  }

  onSaveDeckEmitter(deck: Deck) {
    this.onSaveDeckEvent.emit(deck);
  }

  onDeleteAndResetDeckEmitter(deck: Deck) {
    this.onDeleteAndResetDeckEvent.emit(deck);
  }

  deselectCard(card: Card) {
    this.cardList.push(card);
    this.refreshBar.next(this.currentDeck);
  }

  selectCard(card: Card) {
    const removedCard = this.currentDeck.addCard(card);
    if (removedCard !== null)  {
      this.cardList.push(removedCard);
    }
    this.refreshBar.next(this.currentDeck);
  }

}
