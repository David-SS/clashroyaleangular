import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleDeckEditorComponent } from './clash-royale-deck-editor.component';

describe('ClashRoyaleDeckEditorComponent', () => {
  let component: ClashRoyaleDeckEditorComponent;
  let fixture: ComponentFixture<ClashRoyaleDeckEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleDeckEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleDeckEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
