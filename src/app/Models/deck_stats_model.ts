/**
 * Provides a Deck object
 */
import {Card} from './card_model';
import {Deck} from './deck_model';

// So as to jQuery works without interferences with semantic-ui...
// import * as $ from 'jquery' does not work because it "overwrite" semantic-ui js functions.
declare var $: any;

export class DeckStats {

  constructor(
    private currentElixirCost: number = 0,
    private currentSpell: number = 0,
    private currentBuilding: number = 0,
    private currentTroops: number = 0,
  ) {
  }

  public checkDeck(currentDeck: Deck) {
    this.resetStats();
    currentDeck.cards.forEach((card: Card) => {
      this.checkElixirCost(card);
      this.checkType(card);
    });
    this.updateBarsProgress(currentDeck);
  }

  resetStats() {
    this.currentElixirCost = 0;
    this.currentTroops = 0;
    this.currentBuilding = 0;
    this.currentSpell = 0;
  }

  checkElixirCost(card: Card) {
    this.currentElixirCost += card.elixir;
  }

  checkType(card: Card) {
    if (card.type === 'Troop') { this.currentTroops++; }
    if (card.type === 'Building') { this.currentBuilding++; }
    if (card.type === 'Spell') { this.currentSpell++; }
  }

  public getMeanElixirCost(currentDeck: Deck): number {
    if (this.currentElixirCost === 0) { return 0.0; }
    return (this.currentElixirCost / currentDeck.getCardsCount());
  }

  public getCurrentTroop(): number {
    return (this.currentTroops / 8 * 100);
  }

  public getCurrentSpell(): number {
    return (this.currentSpell / 8 * 100);
  }

  public getCurrentBuilding(): number {
    return (this.currentBuilding / 8 * 100);
  }

  public updateBarsProgress(currentDeck: Deck) {
    this.updateElixirBar(currentDeck);
  }

  private updateElixirBar(currentDeck: Deck) {
    let elixirBarColor = 'bar';
    if (this.getMeanElixirCost(currentDeck) <= 4) {
      elixirBarColor = 'bar cheapElixir';
    } else if (this.getMeanElixirCost(currentDeck) >= 4 && this.getMeanElixirCost(currentDeck) <= 6) {
      elixirBarColor = 'bar mediumElixir';
    } else {
      elixirBarColor = 'bar expensiveElixir';
    }
    $('#elixirBar').progress({
      percent: (this.getMeanElixirCost(currentDeck) / 8 * 100)
    });
    $('#colorElixirBar').attr('class',  elixirBarColor);
  }
}
