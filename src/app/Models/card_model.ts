/**
 * Provides a Card object
 */
export class Card {
  id: number;
  key: string;
  rarity: string;
  type: string;
  name: string;
  description: string;
  elixir: number;
  imageUrl: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    if (this.key === undefined) {
      this.id = 0;
      this.elixir = 0;
      this.key = 'emptyCard';
      this.imageUrl = '/assets/images/nonselected.png';
    } else {
      this.imageUrl = '/assets/images/' + this.key + '.png';
    }
  }

  getRarityIndex() {
    if (this.rarity === 'Legendary') { return 0; }
    if (this.rarity === 'Epic') {return 1; }
    if (this.rarity === 'Rare') {return 2; }
    return 3;
  }
}
