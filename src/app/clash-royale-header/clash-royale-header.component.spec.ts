import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleHeaderComponent } from './clash-royale-header.component';

describe('ClashRoyaleHeaderComponent', () => {
  let component: ClashRoyaleHeaderComponent;
  let fixture: ComponentFixture<ClashRoyaleHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
