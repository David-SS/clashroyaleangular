import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Deck} from '../Models/deck_model';
import {Card} from '../Models/card_model';

@Component({
  selector: 'app-clash-royale-header',
  templateUrl: './clash-royale-header.component.html',
  styleUrls: ['./clash-royale-header.component.css']
})
export class ClashRoyaleHeaderComponent implements OnInit {

  @Input() deckList: Deck[];
  @Input() cardList: Card[];
  @Input() currentDeck: Deck;
  filterCardList: String[];

  @Output() onSelectedDeckEvent: EventEmitter<Deck>;

  constructor() {
    this.onSelectedDeckEvent = new EventEmitter();
    this.filterCardList = [];
  }

  ngOnInit() {
  }

  onSelectedDeckEmitter(deck: Deck) {
    this.onSelectedDeckEvent.emit(deck);
  }

  onChange(cards: String) {
    if (cards === '') {
      this.filterCardList = [];
    } else {
      this.filterCardList = cards.split(',');
    }
  }

  filterByCard(): Deck[] {
    if (this.filterCardList.length === 0) {
      return this.deckList;
    } else {
      return this.deckList.filter((deck) => {
          let count = 0;
          this.filterCardList.forEach((included_card) => {
            deck.cards.forEach((deck_card) => {
              if (deck_card.key === included_card) { count++; }
            });
          });
          if (count === this.filterCardList.length) { return deck; }
      });
    }
  }
}
