import {Component, Input, OnInit} from '@angular/core';
import {Deck} from '../Models/deck_model';

@Component({
  selector: 'app-clash-royale-deck-preview',
  templateUrl: './clash-royale-deck-preview.component.html',
  styleUrls: ['./clash-royale-deck-preview.component.css']
})
export class ClashRoyaleDeckPreviewComponent implements OnInit {

  @Input() currentDeck: Deck;

  constructor() {
  }

  ngOnInit() {
  }

}
