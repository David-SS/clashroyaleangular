import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleDeckPreviewComponent } from './clash-royale-deck-preview.component';

describe('ClashRoyaleDeckPreviewComponent', () => {
  let component: ClashRoyaleDeckPreviewComponent;
  let fixture: ComponentFixture<ClashRoyaleDeckPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleDeckPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleDeckPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
