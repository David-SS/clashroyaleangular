import {Component} from '@angular/core';
import {Card} from './Models/card_model';
import {Deck} from './Models/deck_model';
import {CardDataService} from './Services/card-data.service';
import {DeckDataService} from './Services/deck-data.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CardDataService, DeckDataService]
})
export class AppComponent {
  title = 'CLASH ROYALE ANGULAR DECK';
  cardList: Card[];
  deckList: Deck[];
  currentDeck: Deck;
  refreshBar: Subject<Deck> = new Subject<Deck>();

  constructor(private cardDataService: CardDataService,
              private deckDataService: DeckDataService) {
    this.initCardList();
    this.getDeckList();
    this.currentDeck = new Deck();
  }

  private initCardList() {
    this.cardList = [];
    this.cardDataService.getAllCards().subscribe(cards => {
      this.cardList = cards;
    });
  }

  private getDeckList() {
    this.deckList = [];
    this.deckDataService.getAllDecks().subscribe(decks => {
      this.deckList = decks;
    });
  }

  private moveCardsFromDeckToList() {
    this.currentDeck.cards.forEach((card) => {
      if (card.id !== 0) {
        this.cardList.push(card);
      }
    });
  }

  private moveCardsFromListToDeck(deck: Deck) {
    this.currentDeck = new Deck (deck);
    this.currentDeck.cards.forEach((card) => {
      const index: number = this.cardList.findIndex(i => i.key === card.key);
      if (index !== -1) {
        this.cardList.splice(index, 1);
      }
    });
  }

  private resetCurrentDeck() {
    this.moveCardsFromDeckToList();
    this.currentDeck.resetProps();
    this.currentDeck.resetCards();
    this.refreshBar.next(this.currentDeck);
  }

  selectDeck(deck: Deck) {
    this.moveCardsFromDeckToList();
    this.moveCardsFromListToDeck(deck);
    this.refreshBar.next(this.currentDeck);
    window.location.hash = 'editLink';
  }

  saveDeck(deck: Deck) {
    const index: number = this.deckList.findIndex(i => i.id === deck.id);
    if (index !== -1) {
      const answer = confirm('¿Confirmar los cambios?');
      if (answer === true) {
        this.deckDataService.updateDeck(deck).then((response) => {
          this.deckList[index] = response;
        });
        this.resetCurrentDeck();
      }
    } else {
      this.deckDataService.createDeck(deck).then((response) => {
        this.deckList.push(response);
      });
      this.resetCurrentDeck();
    }
  }

  deleteAndResetDeck(deck: Deck) {
    const answer = confirm('¿Esta seguro de que desea descartar/eliminar el mazo?');
    if (answer === true) {
      this.deckDataService.deleteDeck(deck).then((response) => {
        console.log('Deleted Deck');
      });
      const index: number = this.deckList.findIndex(i => i.id === deck.id);
      if (index !== -1) {
        this.deckList.splice(index, 1);
      }
      this.resetCurrentDeck();
    }



  }

}
