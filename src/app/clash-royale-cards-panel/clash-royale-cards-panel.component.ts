import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Deck} from '../Models/deck_model';
import {Card} from '../Models/card_model';
import {Subject} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-clash-royale-cards-panel',
  templateUrl: './clash-royale-cards-panel.component.html',
  styleUrls: ['./clash-royale-cards-panel.component.css']
})

export class ClashRoyaleCardsPanelComponent implements OnInit {

  @Input() currentDeck: Deck;
  @Input() refreshBar: Subject<Deck>;
  @Output() onDeckSelectedCardEvent: EventEmitter<Card>;

  constructor () {
    this.onDeckSelectedCardEvent = new EventEmitter();
  }

  ngOnInit() {
    this.refreshBar.subscribe((deck) => {
      this.currentDeck = deck;
      this.currentDeck.stats.checkDeck(this.currentDeck);
    });
    $('.shape').shape();
  }

  private popElementOfCardList(card: Card) {
    this.currentDeck.removeCard(this.currentDeck.cards.indexOf(card));
  }

  onDeckSelectedCardEmitter(card: Card) {
    if (card.id !== 0) {
      this.popElementOfCardList(card);
      this.onDeckSelectedCardEvent.emit(card);
    }
  }

}
