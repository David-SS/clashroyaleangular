import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleCardsPanelComponent } from './clash-royale-cards-panel.component';

describe('ClashRoyaleCardsPanelComponent', () => {
  let component: ClashRoyaleCardsPanelComponent;
  let fixture: ComponentFixture<ClashRoyaleCardsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleCardsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleCardsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
