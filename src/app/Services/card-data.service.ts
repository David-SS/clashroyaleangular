import { Injectable } from '@angular/core';
import {Card} from '../Models/card_model';
// import { ApiService } from './api.service';
import { FirebaseService } from './firebase.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardDataService {

  // constructor(private api: ApiService) { }
  constructor(private api: FirebaseService) { }

  getAllCards(): Observable<Card[]> {
    return this.api.getAllCards();
  }
}
