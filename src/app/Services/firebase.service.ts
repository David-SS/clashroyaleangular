import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Card} from '../Models/card_model';
import {map} from 'rxjs/operators';
import {Deck} from '../Models/deck_model';
import {Http} from '@angular/http';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private firebase: AngularFireDatabase,
              private http: Http) { }

  public getAllCards(): Observable<Card[]> {
    return this.firebase.list('/cards').valueChanges()
      .pipe(map(cards => {
        return cards.map((card) => new Card(card))
      }));
  }

  public getAllDecks(): Observable<Deck[]> {
    return this.firebase.list('/decks').valueChanges()
      .pipe(map(decks => {
        return decks.map((deck) => new Deck(deck));
      }));
  }

  createDeck(deck: Deck): Promise<Deck> {
    deck.id = Date.now();
    return new Promise<any>((resolve, reject) => {
      const deckToCreate = {
        id: deck.id,
        name: deck.name,
        cards: deck.cards
      };
      this.firebase.list('/decks').update(deck.id.toString(), deckToCreate)
        .then(
          (res) => deck, // resolve(res),
          (err) => reject(err));
    });
  }

  updateDeck(deck: Deck): Promise<Deck> {
    const deckToCreate = {
      name: deck.name,
      cards: deck.cards
    };
    return new Promise<any>((resolve, reject) => {
      this.firebase.list('/decks').update(deck.id.toString(), deckToCreate)
        .then(
          (res) => deck, // resolve(res),
          (err) => reject(err));
    });
  }

  deleteDeck(deck: Deck): Promise<Deck> {
    return new Promise<any>((resolve, reject) => {
      this.firebase.list('/decks').remove(deck.id.toString())
        .then(
          (res) => resolve(res),
          (err) => reject(err));
    });
  }

}
