import { Injectable } from '@angular/core';
import {Deck} from '../Models/deck_model';
// import { ApiService } from './api.service';
import { FirebaseService } from './firebase.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeckDataService {

  // constructor(private api: ApiService) { }
  constructor(private api: FirebaseService) { }

  getAllDecks(): Observable<Deck[]> {
    return this.api.getAllDecks();
  }

  createDeck(deck: Deck): Promise<Deck> {
    return this.api.createDeck(deck);
  }

  updateDeck(deck: Deck): Promise<Deck> {
    return this.api.updateDeck(deck);
  }

  deleteDeck(deck: Deck): Promise<Deck> {
    return this.api.deleteDeck(deck);
  }

}
