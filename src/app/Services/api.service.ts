import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

import {Card} from '../Models/card_model';
import {Deck} from '../Models/deck_model';
import {Observable} from 'rxjs';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: Http
  ) { }

  public getAllCards(): Observable<Card[]> {
    // return this.http.get<Card[]>(API_URL + '/cards');
    return this.http.get(API_URL + '/cards')
      .pipe(map(response => {
          const cards = response.json();
          return cards.map(card => new Card(card));
        })
      );
  }

  public getAllDecks(): Observable<Deck[]> {
    return this.http.get(API_URL + '/decks')
      .pipe(map(response => {
          const decks = response.json();
          return decks.map(deck => {
            deck.cards = deck.cards.map(card => new Card(card));
            return new Deck(deck);
          });
        })
      );
  }

  public createDeck(deck: Deck): Observable<Deck> {
    deck.id = Date.now();
    return this.http
      .post(API_URL + '/decks', deck)
      .pipe(map(response => {
          return new Deck(response.json());
        })
      );
  }

  public updateDeck(deck: Deck): Observable<Deck> {
    return this.http
      .put(API_URL + '/decks/' + deck.id, deck)
      .pipe(map(response => {
          return new Deck(response.json());
        })
      );
  }

  public deleteDeck(deck: Deck): Observable<Deck> {
    return this.http
      .delete(API_URL + '/decks/' + deck.id)
      .pipe(map(response => {
          // Return empty deck
          return new Deck(response.json());
        })
      );
  }

}
