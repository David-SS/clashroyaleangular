import {Component, Input, OnInit} from '@angular/core';
import {Card} from '../Models/card_model';

@Component({
  selector: 'app-clash-royale-card-item',
  templateUrl: './clash-royale-card-item.component.html',
  styleUrls: ['./clash-royale-card-item.component.css']
})
export class ClashRoyaleCardItemComponent implements OnInit {

  @Input() card: Card;
  @Input() imgSize: string;

  constructor() {
  }

  ngOnInit() {
  }

}
