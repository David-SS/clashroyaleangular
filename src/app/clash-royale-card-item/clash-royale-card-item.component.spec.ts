import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClashRoyaleCardItemComponent } from './clash-royale-card-item.component';

describe('ClashRoyaleCardItemComponent', () => {
  let component: ClashRoyaleCardItemComponent;
  let fixture: ComponentFixture<ClashRoyaleCardItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClashRoyaleCardItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClashRoyaleCardItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
